TEMPLATE = subdirs

CHILLINCLIENT_URL = "https://github.com/koala-team/Chillin-CppClient"
CHILLINCLIENT_VERSION = "v1.0.0"

external_deps.target = get-deps
external_deps.depends = FORCE
external_deps.commands += git clone $$CHILLINCLIENT_URL $$PWD/external/ChillinClient

sub-external-ChillinClient-make_first-ordered.depends = get-deps $(sub-external-ChillinClient-make_first-ordered)
export(sub-external-ChillinClient-make_first-ordered.depends)

QMAKE_EXTRA_TARGETS += external_deps sub-external-ChillinClient-make_first-ordered
PRE_TARGETDEPS += get-deps


SUBDIRS += \
    external/ChillinClient \
    Game

CONFIG += ordered
